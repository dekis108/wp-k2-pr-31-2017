﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class AuthenticationController : Controller
    {
        //isto ulgavnom vezbe6

        // GET: Authentification
        public ActionResult Index()
        {
            User user = (User)Session["UserData"];
            return View(user);
        }

        /*
        [HttpPost]
        public ActionResult Login()
        {
            Debug.WriteLine("[DEBUG][{0}] Authenticator Login called.", DateTime.Now);
            return View();
        }
        */

        [HttpPost]
        public ActionResult LoginAttempt(User user)
        {
            Debug.WriteLine("[DEBUG][{0}] Login attempt called.", DateTime.Now);

            List<User> users = (List<User>)HttpContext.Application["users"];
            User potential = users.Find(x => x.Username == user.Username && x.Password == user.Password);
            if (potential is null)
            {
                //wrong credentials
                object msg = "Pogrešni kredencijali.";
                return View("LoginError", msg);
                //return RedirectToAction("Index");

            }
            if (potential.Active != true)
            {
                object msg = "Nalog je suspendovan.";
                return View("LoginError", msg);
            }

            Session["UserData"] = potential;
            //set logged in flag?
            if (user.Role == Role.Admin)
            {
                return RedirectToAction("Index", "Home");
            }
            return RedirectToAction("Index", "Product");
        }
        

        public ActionResult RegisterForm()
        {
            Debug.WriteLine("[DEBUG][{0}] Authenticator RegisterForm() called.", DateTime.Now);
            //vezbe6
            User user = new User();
            Session["UserData"] = user;
            return View(user);
        }

        [HttpPost]
        public ActionResult Register(User user)
        {
            List<User> users = (List<User>)HttpContext.Application["users"];
            user.Role = Role.Customer;
            user.Active = true;
            if (users.Exists(x => x.Username == user.Username))
            {
                //username arleadye exist
                List<string> msgs = new List<string>();
                msgs.Add("Username taken.");
                return View("RegisterError", msgs);
            }


            if (!ModelState.IsValid)
            {
                Debug.WriteLine("[DEBUG][{0}] Invalid registration attempt", DateTime.Now);
                var errors = ModelState.Select(x => x.Value.Errors)
                       .Where(y => y.Count > 0).ToList();
                List<string> errorMsg = new List<string>();
                foreach(var error in errors)
                {
                    foreach(var e in error)
                    {
                        Debug.WriteLine(e.ErrorMessage);
                        errorMsg.Add(e.ErrorMessage);
                    }
                    
                }
                return View("RegisterError", errorMsg);
                
            }
            //succefully crated the account , redirect to logging in page
            Debug.WriteLine("[DEBUG][{0}] Authenticator Register(User user) called.", DateTime.Now);
            string path = "~/App_Data/UserData.txt";
            Data.SaveUser(user, path);
            users.Add(user);
            HttpContext.Application["users"] = users;

            //Session["UserData"] = user; //wont log in the user right away


            return RedirectToAction("Index");
        }

        public ActionResult LogOut()
        {
            Session["UserData"] = null;

            return RedirectToAction("Index");
        }

    }
}