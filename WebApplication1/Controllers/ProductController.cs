﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ProductController : Controller
    {
        //bool sortedCost = false, sortedName = false, sortedKind = false; 
        // GET: Product
        public ActionResult Index()
        {
            User user = (User)Session["UserData"];
            if (user != null && user.Role == Role.Admin)
            { //redirect admin to their market view
                return RedirectToAction("AdminProduct", "Admin");
            }

            List<Product> products = (List<Product>)HttpContext.Application["product"];
            return View(products);
        }

        [HttpPost]
        public ActionResult SortName()
        {
            bool sortedName = (bool)HttpContext.Application["sortedName"];
            List<Product> products = (List<Product>)HttpContext.Application["product"];
            if (sortedName) //arleady sorted incresing, sort decresing
            {
                products.Reverse();
                HttpContext.Application["sortedName"] = false;
            }
            else
            {
                products = products.OrderBy(x => x.ProductName).ToList();
                HttpContext.Application["sortedName"] = true;
            }
            HttpContext.Application["sortedCost"] = false;
            HttpContext.Application["sortedKind"] = false;
            HttpContext.Application["product"] = products;
            //return View("Index", products);
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult SortCost()
        {
            bool sortedCost = (bool)HttpContext.Application["sortedCost"];
            List<Product> products = (List<Product>)HttpContext.Application["product"];
            if (sortedCost) //arleady sorted incresing, sort decresing
            {
                products.Reverse();
                HttpContext.Application["sortedCost"] = false;
            }
            else
            {
                products = products.OrderBy(x => x.CostKG).ToList();
                HttpContext.Application["sortedCost"] = true;
            }
            HttpContext.Application["sortedName"] = false;
            HttpContext.Application["sortedKind"] = false;
            HttpContext.Application["product"] = products;
            return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult SortKind()
        {
            bool sortedKind = (bool)HttpContext.Application["sortedKind"];
            List<Product> products = (List<Product>)HttpContext.Application["product"];
            if (sortedKind) //arleady sorted incresing, sort decresing
            {
                products.Reverse();
                HttpContext.Application["sortedKind"] = false;
            }
            else
            {
                products = products.OrderBy(x => x.Kind).ToList();
                HttpContext.Application["sortedKind"] = true;
            }
            HttpContext.Application["sortedName"] = false;
            HttpContext.Application["sortedCost"] = false;
            HttpContext.Application["product"] = products;
            return RedirectToAction("Index");
        }

        public ActionResult Orders()
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Customer)
                return RedirectToAction("Index", "Authentication");

            //todo: ucitaj ordere iz fajla i ispisi one koji su za ovaj
            List<Order> orders = (List<Order>)HttpContext.Application["order"];
            List<Order> myOrders = orders.Where(x => x.Username == user.Username).ToList();

            return View(myOrders);
        }

        [HttpPost]
        public ActionResult BuyProduct(string productName)
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Customer)
                return RedirectToAction("Index", "Authentication");

            List<Product> prods = (List<Product>)HttpContext.Application["product"];
            Product prod = prods.Find(x => x.ProductName == productName);
            if (prod is null)
            {
                Debug.WriteLine("[DEBUG][{0}] Incorrect productName: {1}", DateTime.Now, productName);
                return RedirectToAction("Index");
            }
            //returns a view with a form to buy a specific product
            return View(prod);
        }

        [HttpPost]
        public ActionResult MakeOrder()
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Customer)
                return RedirectToAction("Index", "Authentication");

    
            List<Product> prods = (List<Product>)HttpContext.Application["product"];
            Product prod = prods.Find(x => x.ProductName == Request["productName"]);
            double amount = 0;
            string a = Request["amount"];
            if (a is null || a == "") a = "0";
            try
            {
                if (prod is null)
                {
                    throw new ArgumentException("Invalid product name.");
                }
                amount = Double.Parse(a);
                if (amount <= 0)
                {
                    throw new ArgumentException("Kolicina mora biti pozitivna!");
                }
                if (amount > prod.StockAmountKG)
                {
                    throw new ArgumentException("Kolicina mora biti raspoložljiva!");
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e);
                object msg = e.Message;
                return View("OrderError",msg);
            }

            //update stock after purchase
            prod.StockAmountKG -= amount;
            prods[prods.FindIndex(x => x.ProductName == prod.ProductName)] = prod;
            HttpContext.Application["product"] = prods;
            Data.SaveAllProducts("~/App_Data/ProductData.txt", prods);

            //prods[prods.FindIndex(x => x.ProductName == (string)Request["oldProductName"])] = prod;
            Order order = new Order(user.Username, prod.ProductName, prod.FolkName, DateTime.Now, amount, amount * prod.CostKG);
            Data.SaveOrder("~/App_Data/OrderData.txt", order);
            List<Order> orders = Data.LoadOrders("~/App_Data/OrderData.txt");
            HttpContext.Application["order"] = orders;
            return RedirectToAction("Orders");
        }

        [HttpPost]
        public ActionResult SearchName(string nameSearch)
        {
            User user = (User)Session["UserData"];
            if (user != null && user.Role == Role.Admin)
            { //redirect admin to their market view
                return RedirectToAction("AdminProduct", "Admin");
            }

            List<Product> prods = (List<Product>)HttpContext.Application["product"];

            prods = prods.Where(x => x.ProductName.Contains(nameSearch)).ToList();

            return View("Index", prods);
        }

        [HttpPost]
        public ActionResult SearchKind(string kindSearch)
        {
            User user = (User)Session["UserData"];
            if (user != null && user.Role != Role.Customer)
            { //redirect admin to their market view
                return RedirectToAction("AdminProduct", "Admin");
            }

            List<Product> prods = (List<Product>)HttpContext.Application["product"];

            prods = prods.Where(x => x.Kind.Contains(kindSearch)).ToList();

            return View("Index", prods);
        }

        [HttpPost]
        public ActionResult SearchCost(string costLower, string costHigher)
        {
            User user = (User)Session["UserData"];
            if (user != null && user.Role != Role.Customer)
            { //redirect admin to their market view
                return RedirectToAction("AdminProduct", "Admin");
            }

            double low, high;
            try
            {
                low = double.Parse(costLower);
                high = double.Parse(costHigher);
            }
            catch(Exception e)
            {
                Debug.WriteLine(e);
                return RedirectToAction("Index");
            }


            List<Product> prods = (List<Product>)HttpContext.Application["product"];

            prods = prods.Where(x => x.CostKG >= low && x.CostKG <= high).ToList();

            return View("Index", prods);
        }
    }
}