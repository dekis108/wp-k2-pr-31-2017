﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Admin)
                return RedirectToAction("Index", "Authentication"); //if admin redirect to login

            List<User> users = (List<User>)HttpContext.Application["users"];

            return View(users);
        }

        public ActionResult AdminProduct()
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Admin)
                return RedirectToAction("Index", "Authentication");

            List<Product> prods = (List<Product>)HttpContext.Application["product"];

            return View(prods);
        }

        [HttpPost]
        public ActionResult ChangeProduct(string productName)
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Admin)
                return RedirectToAction("Index", "Authentication");

            List<Product> prods = (List<Product>)HttpContext.Application["product"];
            Product prod = prods.Find(x => x.ProductName == productName);
            if (prod is null)
            {
                Debug.WriteLine("[DEBUG][{0}] No product with name: {1}", DateTime.Now, productName);
                return RedirectToAction("AdminProduct");
            }


            return View(prod);
        }

        [HttpPost]
        public ActionResult ConfirmChangeProduct(Product prod)
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Admin)
                return RedirectToAction("Index", "Authentication");

            List<Product> prods = (List<Product>)HttpContext.Application["product"];
            prods[prods.FindIndex(x => x.ProductName == (string)Request["oldProductName"])] = prod;
            HttpContext.Application["product"] = prods;

            Data.SaveAllProducts("~/App_Data/ProductData.txt", prods);

            return RedirectToAction("AdminProduct");
        }

        [HttpPost]
        public ActionResult DeleteOrRestoreUser(string username)
        {
            Debug.WriteLine("[DEBUG][{0}] DeleteOrRestoreUser called for username: {1}",
                DateTime.Now, username);

            List<User> users = (List<User>)HttpContext.Application["users"];

            User user = users.Find(x => x.Username == username);
            if (!(user is null)) {
                user.Active = !user.Active; 
            }
            //users.Where(x => x.Username == username) = user;
            users[users.FindIndex(x => x.Username == user.Username)] = user;
            HttpContext.Application["users"] = users;
            Data.SaveAllUsers(users, "~/App_Data/UserData.txt");
            return RedirectToAction("Index");
        }

        public ActionResult AddProduct()
        {
            User user = (User)Session["UserData"];
            if (user == null || user.Username.Equals("") || user.Role != Role.Admin)
                return RedirectToAction("Index", "Authentication");

            return View();
        }

        [HttpPost]
        public ActionResult SubmitProduct(Product prod)
        {
            List<Product> prods = (List<Product>)HttpContext.Application["product"];
            if (prods.Where(x => x.ProductName == prod.ProductName).Count() != 0) {
                Debug.WriteLine("[DEBUG][{0}] Product with name {1} arleady exists.", 
                    DateTime.Now, prod.ProductName);

                List<string> errorMsgs = new List<string>();
                errorMsgs.Add("Product with name " + prod.ProductName + " arleady exists.");
                return View("NewProductError", errorMsgs);
            }
            if (!ModelState.IsValid)
            {
                List<string> errorMsgs = new List<string>();
                var errors = ModelState.Select(x => x.Value.Errors)
                 .Where(y => y.Count > 0).ToList();
                foreach (var error in errors)
                {
                    foreach (var e in error)
                    {
                        Debug.WriteLine(e.ErrorMessage);
                        errorMsgs.Add(e.ErrorMessage);
                    }

                }

                return View("NewProductError", errorMsgs);
            }


            prods.Add(prod);
            HttpContext.Application["product"] = prods;
            Data.SaveProduct("~/App_Data/UserData.txt", prod);

            return RedirectToAction("AdminProduct");
        }
    }
}