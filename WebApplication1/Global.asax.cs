﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebApplication1.Models;

namespace WebApplication1
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            //dve linije kopirane iz vezbi6
            List<User> users = Data.ReadUsers("~/App_Data/UserData.txt");
            HttpContext.Current.Application["users"] = users;

            List<Product> products = Data.LoadProducts("~/App_Data/ProductData.txt");
            HttpContext.Current.Application["product"] = products;

            List<Order> orders = Data.LoadOrders("~/App_Data/OrderData.txt");
            HttpContext.Current.Application["order"] = orders;

            HttpContext.Current.Application["sortedName"] = false;
            HttpContext.Current.Application["sortedCost"] = false;
            HttpContext.Current.Application["sortedKind"] = false;


        }
    }
}
