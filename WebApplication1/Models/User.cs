﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public enum Gender { Male = 0, Female};
    public enum Role { Admin = 0, Customer}

    public class User
    {
        [Required]
        [DataType(DataType.Text)]
        [StringLength(30, MinimumLength =3)]
        public string Username { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(30, MinimumLength = 8)]
        public string Password { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(30, MinimumLength = 1)]
        public string Name { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(30, MinimumLength = 1)]
        public string LastName { get; set; }

        [Required]
        public Gender Gender { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [EmailAddressAttribute]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime DateOfBirth { get; set; }

        public Role Role { get; set; }

        public bool Active { get; set; }

        public User()
        {

        }

        public User(string username, string pass, string name, string lastname, Gender gender,
                string email, DateTime dob, Role role, bool active)
        {
            Username = username;
            Password = pass;
            Name = name;
            LastName = lastname;
            Gender = gender;
            Email = email;
            DateOfBirth = dob;
            Role = role;
            Active = active;
        }
    }
}