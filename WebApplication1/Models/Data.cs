﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Hosting;

namespace WebApplication1.Models
{
    public class Data
    {
        //read users iz vezbi 6
        public static List<User> ReadUsers(string path)
        {
            List<User> users = new List<User>();
            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);
            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split(';');
                //User p = new User(tokens[0], tokens[1], (UserType)Enum.Parse(typeof(UserType), tokens[2]), int.Parse(tokens[3]));
                Gender g;
                DateTime dob;
                Role role;
                bool active;
                try
                {
                    g = (Gender)Enum.Parse(typeof(Gender), tokens[4]);
                    dob = DateTime.Parse(DateTime.Parse(tokens[6]).ToString("dd/MM/yyyy"));
                    role = (Role)Enum.Parse(typeof(Role), tokens[7]);
                    switch(tokens[8])
                    {
                        case "True":
                            active = true;
                            break;
                        case "False":
                            active = false;
                            break;
                        default:
                            throw new ArgumentException("Activity string is incorrect.");
                    }
                }
                catch(Exception e)
                {
                    Debug.WriteLine(e);
                    continue; //ignore this user and try loading in the next one
                }
                User user = new User(tokens[0], tokens[1], tokens[2], tokens[3], g, tokens[5],dob, role, active);
                users.Add(user);
            }
            sr.Close();
            stream.Close();
            Debug.WriteLine("[DEBUG][{0}] User file loaded.", DateTime.Now);
            return users;
        }

        public static void SaveAllUsers(List<User> users, string path)
        {
            
            path = HostingEnvironment.MapPath(path);
            File.Delete(path);
            FileStream stream = new FileStream(path, FileMode.Append);
            using (StreamWriter sw = new StreamWriter(stream))
            {
                foreach(User user in users)
                {
                    sw.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7};{8}",
                        user.Username, user.Password, user.Name, user.LastName, user.Gender.ToString(),
                        user.Email, user.DateOfBirth, user.Role.ToString(), user.Active.ToString());

                    Debug.WriteLine("[DEBUG][{0}] User:{1} {2} {3} {4} {5} {6} {7} {8} {9} " +
                        "saved to file.", DateTime.Now, user.Username, user.Password, user.Name, user.LastName, user.Gender.ToString(),
                            user.Email, user.DateOfBirth, user.Role.ToString(), user.Active.ToString());
                }
            }
            stream.Close();

        }

        public static void SaveUser(User user, string path)
        {
            using (StreamWriter sw = new StreamWriter(HostingEnvironment.MapPath(path), true))
            {
                sw.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7};True",
                       user.Username, user.Password, user.Name, user.LastName, user.Gender.ToString(),
                       user.Email, user.DateOfBirth, Role.Customer.ToString());

                Debug.WriteLine("[DEBUG][{0}] User:{1} {2} {3} {4} {5} {6} {7} {8} " +
                    "saved to file.", DateTime.Now, user.Username, user.Password, user.Name, user.LastName, user.Gender.ToString(),
                        user.Email, user.DateOfBirth, Role.Customer.ToString());
            }
        }

        public static List<Product> LoadProducts(string path)
        {
            List<Product> prods = new List<Product>();

            string line;
            using(StreamReader sr = new StreamReader(HostingEnvironment.MapPath(path)))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    string[] tokens = line.Split(';');
                    double cost, stock;
                    try
                    {
                        cost = Double.Parse(tokens[6]);

                        stock = Double.Parse(tokens[7]);
                    }
                    catch(Exception e)
                    {
                        Debug.WriteLine(e);
                        continue;
                    }
                    
                    Product prod = new Product(tokens[0], tokens[1], tokens[2], tokens[3], tokens[4],
                            tokens[5], cost, stock);

                    prods.Add(prod);
                }

                Debug.WriteLine("[DEBUG][{0}] Products file loaded.", DateTime.Now);
                return prods;
            }
        }

        public static void SaveAllProducts(string path, List<Product> prods)
        {
            path = HostingEnvironment.MapPath(path);
            File.Delete(path);
            FileStream stream = new FileStream(path, FileMode.Append);
            using (StreamWriter sw = new StreamWriter(stream))
            {
                foreach (Product prod in prods)
                {
                    sw.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7}",
                        prod.ProductName, prod.Kind, prod.FolkName, prod.Color, prod.Origin, prod.Description,
                        prod.CostKG, prod.StockAmountKG);

                    Debug.WriteLine("[DEBUG][{0}] Prod: {0};{1};{2};{3};{4};{5};{6};{7}",
                        prod.ProductName, prod.Kind, prod.FolkName, prod.Color, prod.Origin, prod.Description,
                        prod.CostKG, prod.StockAmountKG);

                }
            }
            stream.Close();
        }

        public static void SaveProduct(string path, Product prod)
        {
            using (StreamWriter sw = new StreamWriter(HostingEnvironment.MapPath(path), true))
            {
                sw.WriteLine("{0};{1};{2};{3};{4};{5};{6};{7}",
                    prod.ProductName, prod.Kind, prod.FolkName, prod.Color, prod.Origin, prod.Description,
                    prod.CostKG, prod.StockAmountKG);

                Debug.WriteLine("[DEBUG][{0}] Prod: {0};{1};{2};{3};{4};{5};{6};{7}",
                    prod.ProductName, prod.Kind, prod.FolkName, prod.Color, prod.Origin, prod.Description,
                    prod.CostKG, prod.StockAmountKG);
            }
        }

        public static void SaveOrder(string path, Order order)
        {
            using (StreamWriter sw = new StreamWriter(HostingEnvironment.MapPath(path), true))
            {
                sw.WriteLine("{0};{1};{2};{3};{4};{5}",order.Username, order.ProductName,
                    order.FolkName, order.TimeOfOrder.ToString(), order.AmountKG, order.Cost);

                Debug.WriteLine("[DEBUG][{0}] Prod: {1};{2};{3};{4};{5};{6}",DateTime.Now, order.Username,
                    order.ProductName,order.FolkName, order.TimeOfOrder.ToString(), order.AmountKG, order.Cost);
            }
        }

        public static List<Order> LoadOrders(string path)
        {
            List<Order> orders = new List<Order>();
            //List<User> users = (List<User>)HttpContext.Current.Application["users"];
            //List<Product> prods = (List<Product>)HttpContext.Current.Application["product"];

            string line;
            using (StreamReader sr = new StreamReader(HostingEnvironment.MapPath(path)))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    string[] tokens = line.Split(';');
                    DateTime time;
                    double amount, cost;
                    try
                    {
                        time = DateTime.Parse(DateTime.Parse(tokens[3]).ToString("dd/MM/yyyy"));
                        amount = double.Parse(tokens[4]);
                        cost = double.Parse(tokens[5]);
                    }
                    catch (Exception e)
                    {
                        Debug.WriteLine(e);
                        continue;
                    }

                    Order order = new Order(tokens[0],tokens[1],tokens[2] , time, amount, cost);

                    orders.Add(order);
                }

                Debug.WriteLine("[DEBUG][{0}] Orders file loaded.", DateTime.Now);
                return orders;
            }
        }

    }
}