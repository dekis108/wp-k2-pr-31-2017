﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Product
    {

        [Required]
        [StringLength(30, MinimumLength = 3)]
        [DataType(DataType.Text)]
        public string ProductName { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(30, MinimumLength = 1)]
        public string Kind { get; set; }
        

        [StringLength(30)]
        [DataType(DataType.Text)]
        public string FolkName { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 1)]
        public string Color { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 1)]
        public string Origin { get; set; }

        [StringLength(100)]
        [DataType(DataType.Text)]
        public string Description { get; set; }

        [Required]
        public double CostKG { get; set; }

        [Required]
        public double StockAmountKG { get; set; }

        
        public Product()
        {
        }

        public Product(string prodName, string kind, string folk, string color, string origin, 
                string description, double cost, double stock)
        {
            ProductName = prodName;
            Kind = kind;
            FolkName = folk;
            Color = color;
            Origin = origin;
            Description = description;
            CostKG = cost;
            StockAmountKG = stock;
        }
    }
}