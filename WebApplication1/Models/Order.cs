﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Order
    {
        //public User Customer { get; set; }
        //public Product Product { get; set; }
        public string Username { get; set; }
        public string ProductName { get; set; }
        public string FolkName { get; set; }

        public DateTime TimeOfOrder { get; set; }
        public double AmountKG { get; set; }
        public double Cost { get; set; }

        public Order()
        {

        }

        public Order(string username, string name, string folk, DateTime time, double amount, double cost)
        {
            //Customer = user;
            //Product = prod;
            Username = username;
            ProductName = name;
            FolkName = folk;
            TimeOfOrder = time;
            AmountKG = amount;
            Cost = cost;
        }
    }
}